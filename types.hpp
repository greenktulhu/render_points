typedef struct {
  unsigned char r, g, b;
} ColorLDR;

const ColorLDR Black = {0, 0, 0};
const ColorLDR White = {255, 255, 255};
const ColorLDR Gray = {127, 127, 127};

typedef struct {
  double r, g, b;
  double brightness;
} ColorHDR;

typedef struct {
  float x, y, z;
} v3;

typedef struct {
  v3 p;
  ColorLDR c;
} Point;
