#define print(a) std::cout << #a << ": " << a << "\n";

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <cassert>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <iostream>
#include <vector>

#include "types.hpp"
#include "functions.hpp"

const int w = 500;
const int h = 500;
const int ch = 3;
const int array_size = w * h;
const int min_z = 100;
const int max_z = 200;

int main() {
  std::srand(std::time(nullptr));
  // std::srand(12353);
  std::vector<Point> points;
  for (int i = 0; i < 2; ++i) {
    points.push_back({{rand_float(0, w), rand_float(0, h), rand_float(min_z, max_z)}, rand_ColorLDR()});
  }

  std::vector<ColorLDR> pixels(array_size, {0, 0, 0});
  smooth(w, h, points, pixels);

  // draw_points(w, h, points, pixels, 1);

  if (!stbi_write_png("smooth.png", w, h, ch, pixels.data(), w * sizeof(ColorLDR)))
    std::cout << "Write error!" << "\n";

  return 0;
}
