int pixel_index(const int &w, const int &x, const int &y) {
  return (w * y) + (x);
}

v3 v3_from_ints(int x, int y, int z) {
  return {static_cast<float>(x), static_cast<float>(y), static_cast<float>(z)};
}

double dist_xy(const v3 &p1, const v3 &p2) {
  double d = pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2);
  d = sqrt(d);
  return d;
}

int rand_int(int a, int b) {
  int range = b - a;
  assert(range != 0);

  return (std::rand() % range) + a;
}

float rand_float(float a, float b) {
  int range = b - a;
  assert(range != 0);
  return {(std::rand() % range) + a};
}

unsigned char rand_unsigned_char() {
  return static_cast<unsigned char>(rand_int(0, 255));
}

ColorLDR rand_ColorLDR() {
  return {rand_unsigned_char(), rand_unsigned_char(), rand_unsigned_char()};
}

void draw_points(const int w, const int h, const std::vector<Point> &points,
                 std::vector<ColorLDR> &pixels, const int &size) {
  for (size_t i = 0; i < points.size(); ++i) {
    Point p = points.at(i);
    int px = p.p.x;
    int py = p.p.y;
    for (int x = px - size; x < px + size; x++) {
      for (int y = py - size; y < py + size; y++) {
        if ((x >= 0 && x < w) && (y >= 0 && y < h)) {
          pixels.at(pixel_index(w, x, y)) = {0, 0, 0};
        }
      }
    }
  }
}

void voronoi(const int w, const int h, const std::vector<Point> &points,
             std::vector<ColorLDR> &pixels) {
  for (int y = 0; y < h; y++) {
    for (int x = 0; x < w; x++) {
      int i = pixel_index(w, x, y);
      v3 current_coords = v3_from_ints(x, y, 0);
      int closest_i = 0;
      float closest_dist = MAXFLOAT;
      for (size_t i = 0; i < points.size(); ++i) {
        Point p = points.at(i);
        float d = dist_xy(current_coords, p.p);
        if (d < closest_dist) {
          closest_i = i;
          closest_dist = d;
        }
      }
      ColorLDR col = points.at(closest_i).c;
      pixels.at(i) = col;
    }
  }
}

void smooth(const int w, const int h, const std::vector<Point> &points,
            std::vector<ColorLDR> &pixels) {
  double max_dist = 1.0f;
  for (int y = 0; y < h; y++) {
    for (int x = 0; x < w; x++) {
      v3 current_coords = v3_from_ints(x, y, 0);
      for (size_t i = 0; i < points.size(); ++i) {
        Point curr_point = points.at(i);
        float curr_dist = dist_xy(current_coords, curr_point.p);
        if (curr_dist > max_dist)
          max_dist = curr_dist;
      }
    }
  }
  for (int y = 0; y < h; y++) {
    for (int x = 0; x < w; x++) {
      int ind = pixel_index(w, x, y);
      v3 current_coords = v3_from_ints(x, y, 0);
      double weights_sum = 0;
      double red_sum = 0;
      double green_sum = 0;
      double blue_sum = 0;
      for (size_t i = 0; i < points.size(); ++i) {
        Point curr_point = points.at(i);
        double curr_dist = dist_xy(current_coords, curr_point.p);
        double weight = (max_dist - curr_dist) * max_dist;
        int power = 8;
        weight = pow(weight, power);
        weights_sum += weight;
        red_sum += curr_point.c.r * weight;
        green_sum += curr_point.c.g * weight;
        blue_sum += curr_point.c.b * weight;
      }

      red_sum = (red_sum / weights_sum);
      green_sum = (green_sum / weights_sum);
      blue_sum = (blue_sum / weights_sum);
      pixels.at(ind) = {static_cast<unsigned char>(red_sum),
                        static_cast<unsigned char>(green_sum),
                        static_cast<unsigned char>(blue_sum)};
    }
  }
}