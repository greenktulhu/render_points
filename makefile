CC=g++
CFLAGS=-Wall
LDFLAGS=
SOURCES=main.cpp
EXECUTABLE=main.out

all:
	$(CC) $(SOURCES) $(CFLAGS) $(LDFLAGS) -o $(EXECUTABLE)
	@echo " "
